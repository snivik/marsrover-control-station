# Server-side Software #

This is the Control Station Part that needs to be launched on The server machine. 

## Communication Hub ##

### About ###

This is the center that hosts an UDP server that receives updates on all components. It also creates an HTTP Server that listens on the port 1337 and responds with the JSON with values

### How to run? ###

You will need **NodeJS**. Install it. Then, go to the root folder with the CMD/sh/bash. Then run the server with  *node dataHub.js* .

If everything is working:
http://localhost:1337/?items=lat,long   should return a JSON with corresponding data.


# Unit Tests #

## Location Reporter ##

### About ###

Reports Fake latitude and longitude to the hub every second. Simple as that.

### How to run? ###

*node locationReporter.js* . You should see no output, but location should change randomly within 0.5 of the previous