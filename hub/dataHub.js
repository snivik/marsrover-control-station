var dgram = require('dgram');
var server = dgram.createSocket('udp4');
var url = require('url');

/*-------------------------------*/
/* GLOBAL DATA */
var secret = "abcd"
var password = "efgh"

/* Delims */
var command_delim = '|'
var key_delim = ':'

/* Misc */
var udp_port = 41234;

/* END OF GLOBAL DATA */
/*-------------------------------*/


var components = [
	// Sample Object for the templating
	{id: 'sample', value: '2000'},
	{id: 'lat', value: '0'},
	{id: 'long', value: '0'},

	// Rover wheels
	{id: 'wheel_lf', value: '0'},
	{id: 'wheel_lm', value: '0'},
	{id: 'wheel_lb', value: '0'},

	{id: 'wheel_rf', value: '0'},
	{id: 'wheel_rm', value: '0'},
	{id: 'wheel_rb', value: '0'},

	// Pitch and Yaw
	{id: 'rover_pitch', value: '0'},
	{id: 'rover_yaw', value: '0'},
    {id: 'rover_roll', value: '0'},

    // Pitch and Yaw
    {id: 'ee_pitch', value: '0'},
    {id: 'ee_yaw', value: '0'},
    {id: 'ee_roll', value: '0'},

    // Pitch and Yaw
    {id: 'uh_pitch', value: '0'},
    {id: 'uh_yaw', value: '0'},
    {id: 'uh_roll', value: '0'},

    // Pitch and Yaw
    {id: 'lh_pitch', value: '0'},
    {id: 'lh_yaw', value: '0'},
    {id: 'lh_roll', value: '0'},


	// TODO Arm stuff
]

// Update the corresponding command
for (var j=0;j<components.length;j++){
	console.log(components[j].id+": "+components[j].value);
}

/*
* Processes message which
* Should be generally an update for the 
* Status of the rover
*/
function processMessage(msg, rinfo){
  var commands = String(msg).split(command_delim);

  // Only accept | delimitered parts
  if (commands.length > 0){

  	  // Get the first pair, it should be key and password
  	  if (commands[0] != secret+key_delim+password){
  	  	console.log('Alien attempting to send a command')
  	  	return;
  	  }

  	  // Process all the variable updates
	  for (var i=1;i<commands.length;i++){
	  	
	  	var command_parts = commands[i].split(key_delim);

	  	// Only accept updates of size 2
	  	if (command_parts.length == 2){
			
			// Update the corresponding command
			for (var j=0;j<components.length;j++){
				if (components[j].id == command_parts[0]){
					components[j].value = command_parts[1];
				}
			}

	  	} else {
	  		console.log('Incorrect Update Command');
	  	}

	  }


  } else {

  }
}





/*
* Error handling for the datagram socket
* That is sick
*/
server.on("error", function(err){
	console.log('err');
})


/**
* Init function on the socket 
*/
server.on("listening", function(){
	console.log('Started Listening server');
	console.log(server.address().address+":"+server.address().port);
})

/**
* Message processing
*/
server.on("message", processMessage );




/**
* Javascript Server the responds with the status of each
* Element. 
*/
var express = require('express')
var app = express()



app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});



app.get('/', function (req, res) {
  
	var requested_parts = new Array();
	var url_parts = url.parse(req.url, true);
	var items = url_parts.query.items;

	// Build the response_items
	var response_items = new Array();

	

	// If request specified what it wants, give only required data
	if (items){
		
		var items_parts = items.split(',');
		// For each requested item find the corresponding value in the public field
		items_parts.forEach(function(item){

			// Find the field being requested
			for (var i=0;i<components.length;i++){

				// Add the component
				if (components[i].id == item){
					response_items.push(components[i]);	
				}
			}
		});

		
	// When there is no data on what to return, send everything
	} else {

		for (var i=0;i<components.length;i++){
			response_items.push(components[i]);
		}

	}
  		
  		res.json(
  		{
  			items: response_items
  			//TODO Some data about the request
		})

  	})

var http_server = app.listen(1338, function () {

  console.log('HTTP Service started on port 1338');

})

// Start Listening the UDP Part
server.bind(udp_port)


