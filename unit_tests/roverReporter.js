
/* Global Data */
var host = 'localhost'
var port = 41234

/* Initial Position Data */
var pitch = 0.0
var roll = 0.0
var yaw = 0.0

// The amount for latitude and longitude a rover can move per second
var step = 0.1


/* Start of the Program */
var dgram = require('dgram');
var client = dgram.createSocket("udp4");


/*
* Increments location by arbitrary value... each second
*/
function updateLocation(){
	
	// Update position
	pitch += (Math.random() * step * 2) - step
	roll += (Math.random() * step * 2) - step
	yaw += (Math.random() * step * 2) - step
	// Prepare & send the message
	var message = new Buffer("abcd:efgh|rover_pitch:"+pitch+"|rover_yaw:"+yaw+"|rover_roll:"+roll);
	client.send(message, 0, message.length, port, host, function(err, bytes) {
	  if (err){
	  	console.log(err)
	  } else {
	  	//console.log('Message sent')
	  }
	  setTimeout(updateLocation, 50);
	});

}

setTimeout(updateLocation, 50);