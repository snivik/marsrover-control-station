
/* Global Data */
var host = 'localhost'
var port = 41234

/* Initial Position Data */
var latitute = 0
var longitude = 0

// The amount for latitude and longitude a rover can move per second
var step = 1.0


/* Start of the Program */
var dgram = require('dgram');
var client = dgram.createSocket("udp4");


/*
* Increments location by arbitrary value... each second
*/
function updateLocation(){
	
	// Update position
	latitute += (Math.random() * step * 2) - step
	longitude += (Math.random() * step * 2) - step

	// Prepare & send the message
	var message = new Buffer("abcd:efgh|lat:"+latitute+"|long:"+longitude);
	client.send(message, 0, message.length, port, host, function(err, bytes) {
	  if (err){
	  	console.log(err)
	  } else {
	  	//console.log('Message sent')
	  }
	  setTimeout(updateLocation, 1000);
	});

}

setTimeout(updateLocation, 1000);